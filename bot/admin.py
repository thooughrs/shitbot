from django.contrib import admin
from bot.models import Profile, Message
from .forms import ProfileForm

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('iduser', 'username', 'name')
    form = ProfileForm


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'room', 'status',  'textadj', 'created_at')


