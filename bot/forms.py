from django import forms

from .models import Profile

class ProfileForm(forms.ModelForm):

    class Meta:
        model = Profile
        fields = (
            'iduser',
            'name',
        )
        widgets = {
            'name': forms.TextInput,
        }