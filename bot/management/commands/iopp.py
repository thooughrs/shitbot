import random
import requests
from datetime import timedelta, timezone
from django.conf import settings
from django.core.management.base import BaseCommand
from telegram import Bot
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, ConversationHandler
from telegram.utils.request import Request
from bot.models import Profile, Message
FIRST, SECOND, THIRD, dirt4, dirt3 = range(5)
ONE, TWO, THREE, FOUR, FIVE, rm1c, rm1d, rm2c, rm2d, rm3c, rm3d, rm4c, rm4d, rm5c, rm5d, photo, back_menu = range(17)

t = timezone(timedelta(hours=6, minutes=0), "Asia/Almaty")

rooms = {
    '1': "Левый писуар",
    '2': "Правый писуар",
    '3': "Левая кабинка",
    '4': "Правая кабинка",
    '5': "не выбрал еще",
}

to_send = '-584164141'
hrs = '831103118'
nura = '445951537'


def log_errors(f):
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:

            error_message = f'Произошла ошибка: {e}'
            print(error_message)
            raise e

    return inner


# @log_errors
# def random(update: Update, context: CallbackContext):
#     user_count = Profile.objects.all().count()
#     random_count = random.randint(0, user_count)
#     random_user = Profile.objects.filter(id=random_count).first()
#
#     if random_user.username is None:
#         context.bot.sendMessage(to_send,
#                                 f'Ответсвенный за просмотр видеокамеры и подтвержние/опровежение - {random_user.name}')
#     else:
#         context.bot.sendMessage(to_send,
#                                 f'Ответсвенный за просмотр видеокамеры и подтвержние/опровежение - @{random_user.username}')

req_url = 'https://team-backend.jcloud.kz/users/staff/bot/'

@log_errors
def start(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    chat_id = update.effective_chat.id
    username = update.message.chat.username
    active = requests.get(f'{req_url}{chat_id}/')
    data = active.json()
    if data['permission']:
        p, _ = Profile.objects.get_or_create(
            iduser=chat_id,
            defaults={
                'username': username,
                'name': name,
            }
        )
        keyboard = [
            [
                InlineKeyboardButton("Левая кабинка", callback_data=str(THREE)),
            ],
            [
                InlineKeyboardButton("Правая кабинка", callback_data=str(FOUR)),
            ],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text(f'{name} Выбери куда ходил:', reply_markup=reply_markup)
        return FIRST
    else:
        context.bot.sendMessage(
            chat_id,
            text=f'{name} вы не найдены')


@log_errors
def start_back(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    query = update.callback_query
    query.answer()
    keyboard = [
        [
            InlineKeyboardButton("Левая кабинка", callback_data=str(THREE)),
        ],
        [
            InlineKeyboardButton("Правая кабинка", callback_data=str(FOUR)),
        ],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text=f'{name} Выбери куда ходил:', reply_markup=reply_markup
    )
    return FIRST


@log_errors
def statusrm3(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    keyboard = [
        [
            InlineKeyboardButton("Чисто", callback_data=str(rm3c)),

        ],
        [
            InlineKeyboardButton("Грязно", callback_data=str(rm3d)),

        ],
        [
            InlineKeyboardButton("<<назад", callback_data=str(back_menu)),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text='Левая кабинкa STATUS', reply_markup=reply_markup
    )

    return SECOND


@log_errors
def rm3_dirty(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    name = update.effective_user.first_name
    profile = Profile.objects.filter(name=name).first()
    iduser = update.effective_chat.id

    query.edit_message_text(
        text=f'{name}, фото?',
    )

    lastroom = Message.objects.filter(room='3')
    last = lastroom.order_by("-created_at")[0]
    last_time = last.created_at.astimezone(t)
    reply_text = f'⚠️ WARNING 🚨 \n\nusername: @{profile.username} \nuser: {name} \n \n Сообщил, что  {rooms["3"]} ГРЯЗНЫЙ \n\n До него заходил {last.user} \n {last_time.ctime()} '
    context.bot.send_message(to_send, reply_text)

    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
        defaults={
            'username': profile.username,
            'name': name,
        }
    )
    m = Message(
        user=p,
        room='3',
        status='d',

    )
    m.save()
    return dirt3


@log_errors
def rm3_clean(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    name = update.effective_user.first_name
    profile = Profile.objects.filter(name=name).first()
    iduser = update.effective_chat.id
    query.edit_message_text(
        text=f'{name}, фото?',
    )

    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
        defaults={
            'username': profile.username,
            'name': name,
        },
    )
    m = Message(
        user=p,
        room='3',
        status='c',
    )
    m.save()
    return THIRD


@log_errors
def statusrm4(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    keyboard = [
        [
            InlineKeyboardButton("Чисто", callback_data=str(rm4c)),

        ],
        [
            InlineKeyboardButton("Грязно", callback_data=str(rm4d)),

        ],
        [
            InlineKeyboardButton("<<назад", callback_data=str(back_menu)),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text='Правая кабинка STATUS', reply_markup=reply_markup
    )
    return SECOND


@log_errors
def rm4_dirty(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    name = update.effective_user.first_name
    profile = Profile.objects.filter(name=name).first()
    iduser = update.effective_chat.id

    query.edit_message_text(
        text=f'{name}, фото?',
    )

    lastroom = Message.objects.filter(room='4')
    last = lastroom.order_by("-created_at")[0]
    last_time = last.created_at.astimezone(t)

    reply_text = f'⚠️ WARNING 🚨 \n\nusername: @{profile.username} \nuser: {name} \n \n Сообщил, что  {rooms["4"]} ГРЯЗНЫЙ \n\n До него заходил {last.user} \n {last_time.ctime()} '
    context.bot.send_message(to_send, reply_text)

    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
        defaults={
            'username': profile.username,
            'name': name,
        }
    )
    m = Message(
        user=p,
        room='4',
        status='d',

    )
    m.save()
    return dirt4


@log_errors
def rm4_clean(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    name = update.effective_user.first_name
    profile = Profile.objects.filter(name=name).first()
    iduser = update.effective_chat.id
    query.edit_message_text(
        text=f'{name}, фото?',
    )

    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
        defaults={
            'username': profile.username,
            'name': name,
        }
    )
    m = Message(
        user=p,
        room='4',
        status='c',
    )
    m.save()
    return THIRD


@log_errors
def send(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    iduser = update.effective_chat.id
    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
    )
    message = Message.objects.filter(user=p).last()
    file = context.bot.get_file(update.message.photo[0].file_id)
    message.textadj = file['file_id']
    message.save()
    reply_text2 = f'{name}, понял принял, следующий раз нажми на /start'
    context.bot.sendMessage(iduser, reply_text2)
    return ConversationHandler.END


@log_errors
def not_photo(update: Update, context):
    name = update.effective_user.first_name
    iduser = update.effective_chat.id
    reply_text2 = f'{name}, Я до сих пор жду ФОТО!!!'
    context.bot.sendMessage(iduser, reply_text2)
    return THIRD


@log_errors
def dirt_send4(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    iduser = update.effective_chat.id
    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
    )
    message = Message.objects.filter(user=p).last()
    file = context.bot.get_file(update.message.photo[0].file_id)
    message.textadj = file['file_id']
    message.save()
    lastroom = Message.objects.filter(room='4')
    last = lastroom.order_by("-created_at")[1]

    user_count = Profile.objects.all().count()
    random_count = random.randint(0, user_count)
    random_user = Profile.objects.filter(id=random_count).first()
    me = Profile.objects.filter(iduser=to_send).first()
    mukhtar = Profile.objects.filter(iduser='264181089').first()
    nura = Profile.objects.filter(iduser='445951537').first()
    if random_user == me or mukhtar or nura:
        check = True
        while check:
            random_count = random.randint(0, user_count)
            random_user = Profile.objects.filter(id=random_count).first()
            if random_user == me or mukhtar or nura:
                active = requests.get(f'{req_url}{random_user.iduser}/')
                data = active.json()
                if data['permission']:
                    check = False
    context.bot.sendMessage(to_send, f'Фото от @{p.username} (кто сообщил)')
    context.bot.sendPhoto(to_send, photo=update.message.photo[0].file_id)
    context.bot.sendMessage(to_send, f'Фото от @{last.user.username} (кто был перед)')
    context.bot.send_photo(to_send, photo=last.textadj)
    context.bot.sendMessage(to_send,
                                f'Ответсвенный за просмотр видеокамеры и подтвержние/опровежение - @{data["full_name"]}')

    reply_text2 = f'{name}, понял принял, следующий раз нажми на /start'
    context.bot.sendMessage(iduser, reply_text2)
    return ConversationHandler.END


@log_errors
def dirt_not_photo4(update: Update, context):
    name = update.effective_user.first_name
    iduser = update.effective_chat.id
    reply_text2 = f'{name}, Я до сих пор жду ФОТО!!!'
    context.bot.sendMessage(iduser, reply_text2)
    return THIRD


@log_errors
def dirt_send3(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    iduser = update.effective_chat.id
    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
    )
    message = Message.objects.filter(user=p).last()
    file = context.bot.get_file(update.message.photo[0].file_id)
    message.textadj = file['file_id']
    message.save()
    lastroom = Message.objects.filter(room='3')
    last = lastroom.order_by("-created_at")[1]

    user_count = Profile.objects.all().count()
    random_count = random.randint(0, user_count)
    random_user = Profile.objects.filter(id=random_count).first()
    me = Profile.objects.filter(iduser=to_send).first()
    mukhtar = Profile.objects.filter(iduser='264181089').first()
    nura = Profile.objects.filter(iduser='445951537').first()
    if random_user == me or mukhtar or nura:
        check = True
        while check:
            random_count = random.randint(0, user_count)
            random_user = Profile.objects.filter(id=random_count).first()
            if random_user == me or mukhtar or nura:
                active = requests.get(f'{req_url}{random_user.iduser}/')
                data = active.json()
                if data['permission']:
                    check = False
    if p.username:
        context.bot.sendMessage(to_send, f'Фото от @{p.username} (кто сообщил)')
    else:
        context.bot.sendMessage(to_send, f'Фото от @{p.name} (кто сообщил)')
    context.bot.sendPhoto(to_send, photo=update.message.photo[0].file_id)
    if last.user.username:
        context.bot.sendMessage(to_send, f'Фото от @{last.user.username} (кто был перед)')
    else:
        context.bot.sendMessage(to_send, f'Фото от @{last.user.name} (кто был перед)')
    context.bot.send_photo(to_send, photo=last.textadj)

    context.bot.sendMessage(to_send,
                            f'Ответсвенный за просмотр видеокамеры и подтвержние/опровежение - @{data["full_name"]}')

    reply_text2 = f'{name}, понял принял, следующий раз нажми на /start'
    context.bot.sendMessage(iduser, reply_text2)
    return ConversationHandler.END


def sender(update: Update):
    chat = '831103118'
    update.bot.sendPhoto(chat, photo=id)
    print("sended")


def kick(update: Update, context):
    bot_token = '6813571288:AAFI6xjp-YhvPMacGxP5aFG5VOcjyP_EQuE'
    chat = '831103118'
    url = f'https://api.telegram.org/bot{bot_token}/kickChatMember'

    # Параметры запроса
    params = {
        'chat_id': '-1002000070753',
        'user_id': '409483952'
    }
    response = requests.post(url, data=params)
    context.bot.sendMessage(chat, response.json())


@log_errors
def dirt_not_photo3(update: Update, context):
    name = update.effective_user.first_name
    iduser = update.effective_chat.id
    reply_text2 = f'{name}, Я до сих пор жду ФОТО!!!'
    context.bot.sendMessage(iduser, reply_text2)
    return THIRD


class Command(BaseCommand):
    start = 'Telegram bot'

    def handle(self, *args, **options):
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0,

        )
        bot = Bot(
            request=request,
            token=settings.TOKEN

        )
        print(bot.get_me())
        updater = Updater(
            bot=bot,
            use_context=True,

        )
        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', start),
                          CommandHandler('kick', kick)],
            states={
                FIRST: [
                    CallbackQueryHandler(statusrm3, pattern='^' + str(THREE) + '$'),
                    CallbackQueryHandler(statusrm4, pattern='^' + str(FOUR) + '$'),
                ],
                SECOND: [
                    CallbackQueryHandler(rm3_clean, pattern='^' + str(rm3c) + '$'),
                    CallbackQueryHandler(rm3_dirty, pattern='^' + str(rm3d) + '$'),
                    CallbackQueryHandler(rm4_clean, pattern='^' + str(rm4c) + '$'),
                    CallbackQueryHandler(rm4_dirty, pattern='^' + str(rm4d) + '$'),
                    CallbackQueryHandler(start_back, pattern='^' + str(back_menu) + '$'),

                ],
                THIRD: [
                    # CallbackQueryHandler(send),
                    MessageHandler(Filters.photo, send),
                    MessageHandler(Filters.all, not_photo),

                ],
                dirt4: [
                    # CallbackQueryHandler(send),
                    MessageHandler(Filters.photo, dirt_send4),
                    # MessageHandler(Filters.all, dirt_not_photo4),
                ],
                dirt3: [
                    # CallbackQueryHandler(send),
                    MessageHandler(Filters.photo, dirt_send3),
                    # MessageHandler(Filters.all, dirt_not_photo3),
                ],

            },
            fallbacks=[CommandHandler('start', start),
                       CommandHandler('random', random),
                       MessageHandler(Filters.text, sender),
                       ],

        )

        updater.dispatcher.add_handler(conv_handler)
        updater.start_polling()
        updater.idle()


def send_photo_by_message_id(message_id):
    message = Message.objects.get(id=message_id)
    request = Request(
        connect_timeout=0.5,
        read_timeout=1.0,

    )
    bot = Bot(
        request=request,
        token=settings.TOKEN

    )
    bot.sendPhoto(hrs, photo=message.textadj)
