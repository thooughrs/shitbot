from django.db import models


class Profile(models.Model):
    iduser = models.PositiveIntegerField(

        verbose_name='ID user tg'
    )
    username = models.CharField(
        max_length=20,
        verbose_name='username',
        db_index=True,
    )
    name = models.CharField(
        max_length=20,
        verbose_name='name',
        db_index=True,
    )

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return self.name


class Message(models.Model):
    user = models.ForeignKey(
        Profile,
        verbose_name='Отправитель',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None

    )

    ROOM_CHOICES = (
        ('1', "Левый писуар"),
        ('2', "Правый писуар"),
        ('3', "Левая кабинка"),
        ('4', "Правая кабинка"),
        ('5', "Раковина"),
        ('6', "Не выбрал"),
    )

    STATUS_CHOICES = (
        ('c', "Clean"),
        ('d', "Dirty"),
        ('H', "ПОКА не чекал"),
    )
    room = models.CharField(
        max_length=15,
        choices=ROOM_CHOICES,
        default='6',
    )

    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default='H',
    )

    textadj = models.TextField(
        max_length=150,
        null=True,
    )


    created_at = models.DateTimeField(
        verbose_name='time created',
        auto_now_add=True,

    )

    def __str__(self):
        return f'Message {self.pk} от {self.user}'

    class Meta:
        verbose_name = 'message'
